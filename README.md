# Решение проблем возникающих при работе с Gitlab-selfhosted

## Перенос Gitlab на другой порт
Для переноса Gitlab на другой порт необходимо перейти к редактированию файла с конфигурацией (при редактировании файла на хосте путь до нужного файла может отличаться в зависимости от настройки volumes):
``` bash
vim .../web/etc/gitlab/gitlab.rb
```
Далее необходимо вставить туда (указав вместо {ip} и {port} свои значения):
``` bash
external_url "http://{ip}:{port}/"
```
После этого стоит перезапустить контейнер с гитлабом
``` bash
docker-compose restart {container name or id}
```

## Регистрация раннера и настройка его конфигурации при смене порта
После переноса Gitlab на другой порт и запуске раннера возникают проблемы - раннер виден и запускается, но не может склонировать репозиторий при начале работы с ним (все падает с Network error).

Для правильной работы runner должен быть зарегестрирован на верный url: 
``` bash
docker exec -ti gitlab-runner bash

gitlab-runner register

Enter the GitLab instance URL (for example, https://gitlab.com/):
http://{container/service name}:{port}
Enter the registration token:
***
```
Вместо **{container/service name}** необходимо подставить название сервиса/контейнера на порту которого будет разворачиваться gitlab - в моем случае это контейнер *wg-client*, вот содержание docker-compose:
``` bash
  wg-client:
   build: .
   container_name: wg-client
   restart: unless-stopped
   tty: true
   cap_add:
     - NET_ADMIN
     - SYS_MODULE
   sysctls:
     - net.ipv4.conf.all.src_valid_mark=1
   volumes:
     - ./wgconf:/etc/wireguard
   networks:
      - gitlab_net

  gitlab-web:
    image: gitlab/gitlab-ce:latest
    restart: unless-stopped
    container_name: gitlab
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        ...
    volumes:
    ...
    depends_on:
      - "wg-client"
    network_mode: service:wg-client

```
Из него видно, что контейнер и сам сервис названы wg-client (и я советую придерживаться одинаковых названий для него), также gitlab умеет сам резолвить ip по этому названию (хотя и криво), поэтому в моем случае необходимо вместо url при регистрации указывать **http://wg-client:{port}** где вместо {port} также необходимо вписать свой порт.

Однако полностью это не решает проблему, необходимо также отредактировать конфигурационный файл контейнера с раннером **config.toml**, если редактировать будем на хосте, то путь зависит от настроек volumes:
``` bash
vim .../etc/gitlab-runner/config.toml
```
Там необходимо добавить несколько строк:

clone_url = "http://wg-client:9090" (Указываем тоже самое, что и при регистрации раннера)

pull_policy = ["if-not-present"]

network_mode = "gitlab_gitlab_net" (**{название контейнера с гитлабом}_{название сети в разделе networks внутри docker-compose.yml}** - также можно посмотреть правильное название с помощью команды **docker network ls**)

Итого **config.toml** должен выглядеть вот так:
``` bash
concurrent = 1
check_interval = 0
user = "gitlab-runner"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "de82e1607f0b"
  url = "http://wg-client:9090"
  id = 3
  token = "1PgwRgZWsG7wq_TXasKp"
  token_obtained_at = 2023-02-22T05:36:43Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  clone_url = "http://wg-client:9090"
  [runners.docker]
    tls_verify = false
    image = "ubuntu:22.04"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    network_mode = "gitlab_gitlab_net"
    pull_policy = ["if-not-present"]
    shm_size = 0
```
После этого стоит перезапустить контейнеры с гитлабом и раннером
``` bash
docker-compose down
docker-compose up -d
```
## Настройка уведомлений через почту Gmail
Для правильной работы уведомлений необходимо задать правильную конфигурацию внутри блока environment в docker-compose с гитлабом:
``` bash
  gitlab-web:
    image: gitlab/gitlab-ce:latest
    restart: unless-stopped
    container_name: gitlab
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        gitlab_rails['gitlab_shell_ssh_port'] = 22
        gitlab_rails['smtp_enable'] = true
        gitlab_rails['smtp_address'] = "smtp.gmail.com"
        gitlab_rails['smtp_port'] = 587
        gitlab_rails['smtp_user_name'] = 
        gitlab_rails['smtp_password'] = 
        gitlab_rails['smtp_domain'] = "smtp.gmail.com"
        gitlab_rails['smtp_authentication'] = "login"
        gitlab_rails['smtp_enable_starttls_auto'] = true
        gitlab_rails['smtp_tls'] = false
        gitlab_rails['smtp_openssl_verify_mode'] = 'peer'
    #ports:
     #- "80:80"
     #- "23:22"
    volumes:
      - ./web/etc/gitlab:/etc/gitlab
      - ./web/var/opt/gitlab:/var/opt/gitlab
      - ./web/var/log/gitlab:/var/log/gitlab
    depends_on:
      - "wg-client"
    network_mode: service:wg-client

```
В **smtp_user_name** необходимо указать почтовый адрес (именно gmail), с которого будут высылаться уведомления. В **smtp_password** необходимо указать специальный пароль для приложений. Очень подробная информация о том как его получить есть в инструкции по настройке Redmine в разделе **"Настройка почты"**. ([Инструкция](https://gitlab.com/kuznetsov-as/redmine-selfhosted)) 

Также необходимо быть уверенным в том, что порт 587 открыт. Я столкнулся с тем, что провайдер у которого открыт сервер по дефолту блокирует этот порт (и внутри сервера этого не видно). Обосновывают они это тем, что многие используют его и smtp.gmail для спам рассылок. Открыли мне его только по запросу в техническую поддержку после объяснения зачем мне этот порт нужен и как именно он будет использоваться.
